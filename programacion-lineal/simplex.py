#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import sys
import json


def print_matriz(mct, i):
    p = "\t|"
    for j in range(mct[i].size):
        p = p + str(float("{0:.2f}".format(mct[i,j]))) + "\t|"
    return p


def print_simplex(cj, zk, ck, xk, b, mct, zj,zj_mct, cj_zj, qk):
    fila = "\t \t|Cj \t|"
    for i in range(cj.size):
        fila = fila +str(cj[0,i]) + "\t|"
    print(fila)
    fila = "|Ck \t|Xk \t|B \t|"
    for i in range(zk.size):
        fila = fila +str(zk[0,i]) + "\t" + "|"
    fila = fila + "Qk" + "\t|" 
    print(fila)
    for i in range(ck.size):
        fila = "|"+str(ck[0,i])+"\t|"+str(xk[0,i])+"\t|"+str(b[0,i])+print_matriz(mct, i)+str(qk[0,i])+"\t|"
        print(fila)
    fila = "\t|Zj \t|"+str(zj) + "\t|"
    for i in range(zj_mct.size):
        fila = fila +str(float("{0:.2f}".format(zj_mct[0,i])))  + "\t|"
    print(fila)
    fila = "\t\t|Cj-Zj\t|"
    for i in range(cj_zj.size):
        fila = fila + str(float("{0:.2f}".format(cj_zj[0,i])))+"\t|"
    print(fila)

def calcular_funcional(zj,zj_mct, ck,b,mct):
    zj = 0
    for i in range(ck.size):
        zj = zj + (ck[0,i] * b[0,i])
    for i in range(mct[0].size):
        zj_mct[0,i] = 0
        for j in range(ck.size):
            zj_mct[0,i] = zj_mct[0,i] + (ck[0,j] * mct[j,i])
    return zj, zj_mct


def calcular_costo_oportinidad(cj_zj, cj, zj_mct):
    for i in range(cj.size):
        cj_zj[0,i] = cj[0,i] - zj_mct[0,i]
    
    return cj_zj 


def datos(datos_json):
    file = open(datos_json,'r').read()
    datos_json = json.loads(file)
    # Funcional
    cj = np.matrix(datos_json["cj"])
    # Coeficientes del funcional
    zk = np.matrix(datos_json["zk"])
    # Matriz de Coeficientes Tecnologicos
    mct = np.matrix(datos_json["mct"])
    # B = Cuantifica las variables del funcional
    b = np.matrix(datos_json["b"])
    # Matriz de Coeficientes de las variables basicas
    ck = np.matrix(datos_json["ck"])
    # Variables Basicas
    xk = np.matrix(datos_json["xk"])
    # Matriz valores del funcional
    zj_mct = np.matrix(np.zeros(cj.size))
    print(zj_mct)
    # Valor dela funcion objetivo
    zj = 0.
    # Matriz de costo de Oportunidad
    cj_zj = np.matrix(np.zeros(cj.size))
    # Matriz de Tasa de Agotamiento
    qk = np.matrix(np.zeros(b.size))

    zj, zj_mct = calcular_funcional(zj,zj_mct,ck,b,mct)
    cj_zj = calcular_costo_oportinidad(cj_zj, cj, zj_mct)

    return cj, zk, ck, xk, b, mct, zj,zj_mct, cj_zj, qk


def calcular_tasa_agotamiento(qk,b,mct,entrada_indice):
    for i in range(qk.size):
        if (b[0,i]== 0) or (mct[i,entrada_indice] ==0):
            qk[0,i] = 99999999
        else:
            qk[0,i] = b[0,i]/mct[i,entrada_indice]
            if qk[0,i] <= 0:
                qk[0,i] = 99999999
    return qk



def simplex_dantzig(cj, zk, ck, xk, b, mct, zj,zj_mct, cj_zj, qk, criterio):
    # 1. Busco el valor mayor en la matriz de costo de oportunidad
    if criterio == "max":
        entrada_indice = cj_zj[0].argmax() # columna en matriz de coeficientes tecnologicos
    elif criterio== "min":
        entrada_indice = cj_zj[0].argmin()
    # 2. Busco la variable que sale por el factor de agotamiento
    qk = calcular_tasa_agotamiento(qk,b,mct,entrada_indice)
    # 3. De la tasa de agotamiento me quedo con el minimo
    salida_indice = qk[0].argmin() # fila en matriz de coeficientes tecnologicos
    # 4. Punto Pivote 
    punto_pivote = mct[salida_indice,entrada_indice]
    
    # Valor de Variables Basicas
    ck[0,salida_indice] = cj[0,entrada_indice]
    xk[0,salida_indice] = zk[0,entrada_indice]
    
    # Gauss-Jordan
    for i in range(b.size):
        if i == salida_indice:
            continue
        else:
            b[0,i] = b[0,i] - ((mct[i,entrada_indice]* b[0,salida_indice]) / punto_pivote)

    for i in range(b.size):
        if i == salida_indice:
            continue
        else: 
            for j in range(mct[0].size):
                if j == entrada_indice:
                    continue
                else:
                    mct[i,j] = mct[i,j] -( (mct[i,entrada_indice]* mct[salida_indice,j])/ punto_pivote)
    # Dividir toda la fila por el punto pivote
    b[0,salida_indice] = b[0,salida_indice] / punto_pivote
    
    for i in range(mct[0].size):
        mct[salida_indice,i] = mct[salida_indice,i]/ punto_pivote
    # Trasformar a matriz identidad
    for i in range(b.size):
        if i == salida_indice:
            continue
        else:
            mct[i,entrada_indice] = 0

    # Calculo Zj i Cj-Zj        
    zj, zj_mct = calcular_funcional(zj,zj_mct,ck,b,mct)
    cj_zj = calcular_costo_oportinidad(cj_zj, cj, zj_mct)

    return cj, zk, ck, xk, b, mct, zj,zj_mct, cj_zj, qk 


def resolver(cj, zk, ck, xk, b, mct, zj,zj_mct, cj_zj, qk, datos_json):
    print("\n")
    print("MATRIZ INICIAL:")
    print_simplex(cj, zk, ck, xk, b, mct, zj,zj_mct, cj_zj, qk)
    print("\n\n")
    i = 1
    # Tomo si el criterio es maxiizar o minimizar
    file = open(datos_json,'r').read()
    datos_json = json.loads(file)
    # Criterio
    criterio = datos_json["criterio"]

    if criterio=="max":
        print("Criterio MAX")
        while (cj_zj[0].max() > 0):
            cj, zk, ck, xk, b, mct, zj,zj_mct, cj_zj, qk = simplex_dantzig(cj, zk, ck, xk, b, mct, zj,zj_mct, cj_zj, qk, criterio)
            print("Matrix ",i)
            i += 1
            print_simplex(cj, zk, ck, xk, b, mct, zj,zj_mct, cj_zj, qk)
            print("\n\n")
    elif criterio=="min":
        print("Criterio MIN")
        while (cj_zj[0].min() < 0):
            cj, zk, ck, xk, b, mct, zj,zj_mct, cj_zj, qk = simplex_dantzig(cj, zk, ck, xk, b, mct, zj,zj_mct, cj_zj, qk, criterio)
            print("Matrix ",i)
            i += 1
            print_simplex(cj, zk, ck, xk, b, mct, zj,zj_mct, cj_zj, qk)
            print("\n\n")

    print("Fin")


if __name__ == '__main__':
    cj, zk, ck, xk, b, mct, zj,zj_mct, cj_zj, qk = datos(sys.argv[1])
    resolver(cj, zk, ck, xk, b, mct, zj,zj_mct, cj_zj, qk, sys.argv[1])
