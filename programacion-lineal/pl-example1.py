#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#	Consider the following problem:
#
#	Minimize: f = -1*x[0] + 4*x[1]
#
#	Subject to: -3*x[0] + 1*x[1] <= 6
#
#	    1*x[0] + 2*x[1] <= 4
#	        x[1] >= -3



from scipy.optimize import linprog

c = [-1, 4]
A = [[-3, 1], [1, 2]]
b = [6, 4]
x0_bnds = (None, None)
x1_bnds = (-3, None)
res = linprog(c, A, b, bounds=(x0_bnds, x1_bnds))
print(res)