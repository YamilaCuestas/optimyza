
* Instalar Scipy

```
    (virtualenv)$ pip install scipy
    Collecting scipy
      Downloading scipy-1.0.0-cp34-cp34m-manylinux1_x86_64.whl (47.6MB)
        100% |████████████████████████████████| 47.6MB 38kB/s 
    Collecting numpy>=1.8.2 (from scipy)
      Downloading numpy-1.14.1-cp34-cp34m-manylinux1_x86_64.whl (12.1MB)
        100% |████████████████████████████████| 12.1MB 148kB/s 
    Installing collected packages: numpy, scipy
    Successfully installed numpy-1.14.1 scipy-1.0.0
```

* Para PULP es necesario librerías de linux:

```
    $ apt install glpk-utils
``` 

* Luego en el entorno virtual instalamos pulp

```
    (virtualenv$ pip install pulp
    Collecting pulp
      Downloading PuLP-1.6.8.tar.gz (13.5MB)
        100% |████████████████████████████████| 13.5MB 121kB/s 
    Collecting pyparsing>=2.0.1 (from pulp)
      Using cached pyparsing-2.2.0-py2.py3-none-any.whl
    Installing collected packages: pyparsing, pulp
      Running setup.py install for pulp ... done
    Successfully installed pulp-1.6.8 pyparsing-2.2.0
```


```
    apt install python3-tk
    Leyendo lista de paquetes... Hecho
    Creando árbol de dependencias       
    Leyendo la información de estado... Hecho
    Se instalarán los siguientes paquetes extras:
      blt tk8.6-blt2.5
    Paquetes sugeridos:
      blt-demo tix python3-tk-dbg
    Se instalarán los siguientes paquetes NUEVOS:
      blt python3-tk tk8.6-blt2.5

```
